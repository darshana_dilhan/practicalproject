﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace PracticalTestAPI.Models
{
    public class FolderDbOperation
    {
        public string insertFolders(string folderPath, List<string> filesAndFolders)
        {
            try
            {
                DataTable FolderDetaildataTable = new DataTable("FolderDetail");
                FolderDetaildataTable.Columns.Add("FolderPath", typeof(string));
                FolderDetaildataTable.Columns.Add("File", typeof(string));

                foreach (var item in filesAndFolders)
                {
                    FolderDetaildataTable.Rows.Add(folderPath, item);
                }


                SqlParameter SPparameter = new SqlParameter();
                SPparameter.ParameterName = "@FolderDetails";
                SPparameter.SqlDbType = System.Data.SqlDbType.Structured;
                SPparameter.Value = FolderDetaildataTable;

                string FolderInsertStoredProcedure = "InsertFolderDetail";
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlCommand command = new SqlCommand(FolderInsertStoredProcedure, Connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(SPparameter);
                Connection.Open();
                command.ExecuteNonQuery();
                return "Successfully Inserted";
            }
            catch (Exception exception)
            {
                return exception.ToString() + " Folder Access"; 
            }

        }
    }  
}
