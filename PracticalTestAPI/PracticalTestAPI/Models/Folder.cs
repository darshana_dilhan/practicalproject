﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticalTestAPI.Models
{
    public class FolderDetails
    {
        public List<string> fileAndFolderList { get; set; }
        public string folderPath { get; set; }
    }
}