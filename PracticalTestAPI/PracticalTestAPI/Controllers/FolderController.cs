﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using PracticalTestAPI.Models;


namespace PracticalTestAPI.Controllers
{
    public class FolderController : Controller
    {
        
        public string folderDetailPost(FolderDetails browsedFolder)
        {
            
            try
            {
                List<string> fileAndFolderList = new List<string>();
                foreach (var folderOrfile in browsedFolder.fileAndFolderList)
                {
                    fileAndFolderList.Add(folderOrfile);
                }
                FolderDbOperation folderUpload = new FolderDbOperation();
              var uploadStatus= folderUpload.insertFolders(browsedFolder.folderPath, fileAndFolderList);

                return uploadStatus;
            }
            catch (Exception ex)
            {
                return ex.ToString() + " FolderController";
            }
        }
    }
}