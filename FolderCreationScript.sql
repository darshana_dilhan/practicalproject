IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE Table_name='FolderDetail' )
		BEGIN
			CREATE TABLE [FolderDetail] (
				 ID int IDENTITY(1,1) PRIMARY KEY,
				FolderPath varchar(100),
				[File] varchar(50)
			
			)
		END
	GO