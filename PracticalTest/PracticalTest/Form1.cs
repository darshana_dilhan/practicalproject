﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace PracticalTest
{
    public partial class FolderUploadWindow : Form
    {
        public FolderUploadWindow()
        {
            InitializeComponent();
        }
        private async void btnFolderDetailUpload_Click(object sender, EventArgs e)
        {
            try
            {
                FolderDetail browsedFolder = new FolderDetail();
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.ShowNewFolderButton = true;
                DialogResult dialogResult = folderBrowserDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {

                    browsedFolder.folderPath = folderBrowserDialog.SelectedPath;
                    foreach (var folder in Directory.GetDirectories(folderBrowserDialog.SelectedPath))
                    {
                        browsedFolder.fileAndfolderList.Add(folder);
                    }
                    foreach (var file in Directory.GetFiles(folderBrowserDialog.SelectedPath))
                    {
                        browsedFolder.fileAndfolderList.Add(file);
                    }
                }
                HttpClient httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri("http://localhost:10155/Folder/");
                var JsonSerializedFolderObject = JsonConvert.SerializeObject(browsedFolder);
                var ByteConvertedFolderObject = Encoding.UTF8.GetBytes(JsonSerializedFolderObject);
                var byteArrayFloderObject = new ByteArrayContent(ByteConvertedFolderObject);
                byteArrayFloderObject.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var FolderUploadResult = httpClient.PostAsync("folderDetailPost/", byteArrayFloderObject).Result;

                MessageBox.Show("Folder Uploaded successfully " + FolderUploadResult.ToString());
            }

            catch (Exception exeption)
            {
                MessageBox.Show(exeption.ToString() + " Upload exception");
            }
        }
    }
}
