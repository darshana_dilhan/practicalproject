﻿namespace PracticalTest
{
    partial class FolderUploadWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnFolderDetailUpload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFolderDetailUpload
            // 
            this.btnFolderDetailUpload.Location = new System.Drawing.Point(274, 204);
            this.btnFolderDetailUpload.Name = "btnFolderDetailUpload";
            this.btnFolderDetailUpload.Size = new System.Drawing.Size(132, 71);
            this.btnFolderDetailUpload.TabIndex = 2;
            this.btnFolderDetailUpload.Text = "Upload";
            this.btnFolderDetailUpload.UseVisualStyleBackColor = true;
            this.btnFolderDetailUpload.Click += new System.EventHandler(this.btnFolderDetailUpload_Click);
            // 
            // FolderUploadWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 501);
            this.Controls.Add(this.btnFolderDetailUpload);
            this.Name = "FolderUploadWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnFolderDetailUpload;
    }
}

