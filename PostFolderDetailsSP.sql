USE [FolderDB]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Alter PROCEDURE InsertFolderDetail

	@FolderDetails As [dbo].[TableValuedTypeFolderDetailInsert] Readonly
	 
AS BEGIN

INSERT INTO FolderDetail
(
    
    FolderPath,
    [File]
)
Select FolderPath, [File] From @FolderDetails
END